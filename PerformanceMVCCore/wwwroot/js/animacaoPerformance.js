﻿anime.timeline({ loop: false })
	.add({
		targets: '.ml5 .line',
		opacity: [0.5, 1],
		scaleX: [0, 1],
		easing: "easeInOutExpo",
		duration: 1000
	}).add({
		targets: '.ml5 .line',
		duration: 1000,
		easing: "easeOutExpo",
		translateY: function (e, i, l) {
			var offset = -0.625 + 0.625 * 2 * i;
			return offset + "em";
		}
	}).add({
		targets: '.ml5 .ampersand',
		opacity: [0, 1],
		scaleY: [0.5, 1],
		easing: "easeOutExpo",
		duration: 1000,
		offset: '-=1000'
	//}).add({
	//	targets: '.ml5',
	//	opacity: 0,
	//	duration: 1000,
	//	easing: "easeOutExpo",
	//	delay: 1000
	});