﻿//Função que armazena data e hora
function GetDate() {

    var d = new Date();
    var month = new Array();
    month[0] = "Janeiro";
    month[1] = "Fevereiro";
    month[2] = "Março";
    month[3] = "Abril";
    month[4] = "Maio";
    month[5] = "Junho";
    month[6] = "Julho";
    month[7] = "Agosto";
    month[8] = "Setembro";
    month[9] = "Outubro";
    month[10] = "Novembro";
    month[11] = "Dezembro";

    var weekday = new Array(7);
    weekday[0] = "Domingo";
    weekday[1] = "Segunda-feira";
    weekday[2] = "Terça-feira";
    weekday[3] = "Quarta-feira";
    weekday[4] = "Quinta-feira";
    weekday[5] = "Sexta-feira";
    weekday[6] = "Sábado";

    var wd = weekday[d.getUTCDay()];
    var m = month[d.getMonth()];
    var day = d.getDate();
    var y = d.getFullYear();
    var h = d.getHours()
    var min = d.getMinutes()
    var sec = d.getSeconds();
    var data = wd + ', ' + day + ' de ' + m + ' de ' + y + ',' + h + ':' + min;

    return data;
}
