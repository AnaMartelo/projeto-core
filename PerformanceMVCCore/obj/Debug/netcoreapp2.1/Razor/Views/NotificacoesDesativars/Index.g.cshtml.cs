#pragma checksum "C:\ACADEMIA\new project\PerformanceMVCCore\Views\NotificacoesDesativars\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "95e057d691a0e14043d6d42b6ba0e6fbd6d94aae"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_NotificacoesDesativars_Index), @"mvc.1.0.view", @"/Views/NotificacoesDesativars/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/NotificacoesDesativars/Index.cshtml", typeof(AspNetCore.Views_NotificacoesDesativars_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\_ViewImports.cshtml"
using PerformanceMVCCore;

#line default
#line hidden
#line 2 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\_ViewImports.cshtml"
using PerformanceMVCCore.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"95e057d691a0e14043d6d42b6ba0e6fbd6d94aae", @"/Views/NotificacoesDesativars/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"961db99ca7d6cb714de923693b3a57328cf0f52f", @"/Views/_ViewImports.cshtml")]
    public class Views_NotificacoesDesativars_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<DALCore.Models.NotificacoesDesativar>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Create", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(58, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\NotificacoesDesativars\Index.cshtml"
  
    ViewData["Title"] = "Desativar notificações";

#line default
#line hidden
            BeginContext(118, 53, true);
            WriteLiteral("\r\n<h2>Desativar notificações</h2>\r\n\r\n<p hidden>\r\n    ");
            EndContext();
            BeginContext(171, 37, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c15a30b9300c45ada1bab824924bb037", async() => {
                BeginContext(194, 10, true);
                WriteLiteral("Criar Nova");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(208, 339, true);
            WriteLiteral(@"
</p>
<table class=""table"">
    <thead>
        <tr>
            <th>
                Data de Desativação
            </th>
            <th>
                Assunto da Notificação
            </th>
            <th>
                Utilizador
            </th>
            <th></th>
        </tr>
    </thead>
    <tbody>
");
            EndContext();
#line 28 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\NotificacoesDesativars\Index.cshtml"
         foreach (var item in Model)
        {

#line default
#line hidden
            BeginContext(596, 60, true);
            WriteLiteral("            <tr>\r\n                <td>\r\n                    ");
            EndContext();
            BeginContext(657, 50, false);
#line 32 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\NotificacoesDesativars\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.DataDesativacao));

#line default
#line hidden
            EndContext();
            BeginContext(707, 67, true);
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
            EndContext();
            BeginContext(775, 63, false);
#line 35 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\NotificacoesDesativars\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.IdNotificacaoNavigation.Body));

#line default
#line hidden
            EndContext();
            BeginContext(838, 67, true);
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
            EndContext();
            BeginContext(906, 62, false);
#line 38 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\NotificacoesDesativars\Index.cshtml"
               Write(Html.DisplayFor(modelItem => item.IdUtilizadorNavigation.Nome));

#line default
#line hidden
            EndContext();
            BeginContext(968, 74, true);
            WriteLiteral("\r\n                </td>\r\n                <td hidden>\r\n                    ");
            EndContext();
            BeginContext(1043, 67, false);
#line 41 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\NotificacoesDesativars\Index.cshtml"
               Write(Html.ActionLink("Editar", "Edit", new { /* id=item.PrimaryKey */ }));

#line default
#line hidden
            EndContext();
            BeginContext(1110, 24, true);
            WriteLiteral(" |\r\n                    ");
            EndContext();
            BeginContext(1135, 72, false);
#line 42 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\NotificacoesDesativars\Index.cshtml"
               Write(Html.ActionLink("Detalhes", "Details", new { /* id=item.PrimaryKey */ }));

#line default
#line hidden
            EndContext();
            BeginContext(1207, 24, true);
            WriteLiteral(" |\r\n                    ");
            EndContext();
            BeginContext(1232, 69, false);
#line 43 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\NotificacoesDesativars\Index.cshtml"
               Write(Html.ActionLink("Apagar", "Delete", new { /* id=item.PrimaryKey */ }));

#line default
#line hidden
            EndContext();
            BeginContext(1301, 44, true);
            WriteLiteral("\r\n                </td>\r\n            </tr>\r\n");
            EndContext();
#line 46 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\NotificacoesDesativars\Index.cshtml"
        }

#line default
#line hidden
            BeginContext(1356, 26, true);
            WriteLiteral("    </tbody>\r\n</table>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<DALCore.Models.NotificacoesDesativar>> Html { get; private set; }
    }
}
#pragma warning restore 1591
