#pragma checksum "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f8836974cbedcf68ec23fe0dbec3bd2bc18e9669"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Notificacoes_Delete), @"mvc.1.0.view", @"/Views/Notificacoes/Delete.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Notificacoes/Delete.cshtml", typeof(AspNetCore.Views_Notificacoes_Delete))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\_ViewImports.cshtml"
using PerformanceMVCCore;

#line default
#line hidden
#line 2 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\_ViewImports.cshtml"
using PerformanceMVCCore.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f8836974cbedcf68ec23fe0dbec3bd2bc18e9669", @"/Views/Notificacoes/Delete.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"961db99ca7d6cb714de923693b3a57328cf0f52f", @"/Views/_ViewImports.cshtml")]
    public class Views_Notificacoes_Delete : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<DALCore.Models.Notificacoes>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "hidden", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("linkVoltar"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(36, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
  
	ViewData["Title"] = "Apagar";

#line default
#line hidden
            BeginContext(77, 2208, true);
            WriteLiteral(@"<style>
	#save {
		-moz-box-shadow: inset 0px 0px 17px 2px #8a2a21;
		-webkit-box-shadow: inset 0px 0px 17px 2px #8a2a21;
		box-shadow: inset 0px 0px 17px 2px #8a2a21;
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #c62d1f), color-stop(1, #f24437));
		background: -moz-linear-gradient(top, #c62d1f 5%, #f24437 100%);
		background: -webkit-linear-gradient(top, #c62d1f 5%, #f24437 100%);
		background: -o-linear-gradient(top, #c62d1f 5%, #f24437 100%);
		background: -ms-linear-gradient(top, #c62d1f 5%, #f24437 100%);
		background: linear-gradient(to bottom, #c62d1f 5%, #f24437 100%);
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#c62d1f', endColorstr='#f24437',GradientType=0);
		background-color: #c62d1f;
		-moz-border-radius: 14px;
		-webkit-border-radius: 14px;
		border-radius: 14px;
		border: 1px solid #d02718;
		display: inline-block;
		cursor: pointer;
		color: white;
		font-family: Arial;
		font-size: 15px;
		font-weight: bold;
		pa");
            WriteLiteral(@"dding: 2px 0px;
		text-decoration: none;
		text-align: center;
		text-shadow: 0px 1px 5px #810e05;
		height: 35px;
		width: 90px;
		margin-bottom: 20px;
	}

		#save:hover {
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #f24437), color-stop(1, #c62d1f));
			background: -moz-linear-gradient(top, #f24437 5%, #c62d1f 100%);
			background: -webkit-linear-gradient(top, #f24437 5%, #c62d1f 100%);
			background: -o-linear-gradient(top, #f24437 5%, #c62d1f 100%);
			background: -ms-linear-gradient(top, #f24437 5%, #c62d1f 100%);
			background: linear-gradient(to bottom, #f24437 5%, #c62d1f 100%);
			filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#f24437', endColorstr='#c62d1f',GradientType=0);
			background-color: #f24437;
		}

		#save:active {
			position: relative;
			top: 1px;
		}
</style>
<div id=""boxTable"">
	<div id=""headTituloView"">
		<h2 id=""h2titulo"">Apagar</h2>
	</div>
	<div class=""questionDelete"">
		<h4 class=""certeza"">Tem a c");
            WriteLiteral("erteza que pretende apagar esta notificação?</h4>\r\n\t</div>\r\n\r\n\t<div>\r\n\r\n\t\t<hr />\r\n\t\t<dl class=\"dl-horizontal\">\r\n\t\t\t<dt>\r\n\t\t\t\tDesignação\r\n\t\t\t</dt>\r\n\t\t\t<dd>\r\n\t\t\t\t");
            EndContext();
            BeginContext(2286, 36, false);
#line 70 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
           Write(Html.DisplayFor(model => model.Nome));

#line default
#line hidden
            EndContext();
            BeginContext(2322, 69, true);
            WriteLiteral("\r\n\t\t\t</dd>\r\n\t\t\t<dt>\r\n\t\t\t\tTipo de Notificação\r\n\t\t\t</dt>\r\n\t\t\t<dd>\r\n\t\t\t\t");
            EndContext();
            BeginContext(2392, 47, false);
#line 76 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
           Write(Html.DisplayFor(model => model.TipoNotificacao));

#line default
#line hidden
            EndContext();
            BeginContext(2439, 70, true);
            WriteLiteral("\r\n\t\t\t</dd>\r\n\t\t\t<dt>\r\n\t\t\t\tTipo de Destinatário\r\n\t\t\t</dt>\r\n\t\t\t<dd>\r\n\t\t\t\t");
            EndContext();
            BeginContext(2510, 48, false);
#line 82 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
           Write(Html.DisplayFor(model => model.TipoDestinatario));

#line default
#line hidden
            EndContext();
            BeginContext(2558, 63, true);
            WriteLiteral("\r\n\t\t\t</dd>\r\n\t\t\t<dt>\r\n\t\t\t\tDestinatários\r\n\t\t\t</dt>\r\n\t\t\t<dd>\r\n\t\t\t\t");
            EndContext();
            BeginContext(2622, 44, false);
#line 88 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
           Write(Html.DisplayFor(model => model.Destinatario));

#line default
#line hidden
            EndContext();
            BeginContext(2666, 25, true);
            WriteLiteral("\r\n\t\t\t</dd>\r\n\t\t\t<dt>\r\n\t\t\t\t");
            EndContext();
            BeginContext(2692, 39, false);
#line 91 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
           Write(Html.DisplayNameFor(model => model.Dia));

#line default
#line hidden
            EndContext();
            BeginContext(2731, 25, true);
            WriteLiteral("\r\n\t\t\t</dt>\r\n\t\t\t<dd>\r\n\t\t\t\t");
            EndContext();
            BeginContext(2757, 35, false);
#line 94 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
           Write(Html.DisplayFor(model => model.Dia));

#line default
#line hidden
            EndContext();
            BeginContext(2792, 57, true);
            WriteLiteral("\r\n\t\t\t</dd>\r\n\t\t\t<dt>\r\n\t\t\t\tAssunto\r\n\t\t\t</dt>\r\n\t\t\t<dd>\r\n\t\t\t\t");
            EndContext();
            BeginContext(2850, 39, false);
#line 100 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
           Write(Html.DisplayFor(model => model.Subject));

#line default
#line hidden
            EndContext();
            BeginContext(2889, 55, true);
            WriteLiteral("\r\n\t\t\t</dd>\r\n\t\t\t<dt>\r\n\t\t\t\tTexto\r\n\t\t\t</dt>\r\n\t\t\t<dd>\r\n\t\t\t\t");
            EndContext();
            BeginContext(2945, 36, false);
#line 106 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
           Write(Html.DisplayFor(model => model.Body));

#line default
#line hidden
            EndContext();
            BeginContext(2981, 60, true);
            WriteLiteral("\r\n\t\t\t</dd>\r\n\t\t\t<dt>\r\n\t\t\t\tParâmetros\r\n\t\t\t</dt>\r\n\t\t\t<dd>\r\n\t\t\t\t");
            EndContext();
            BeginContext(3042, 42, false);
#line 112 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
           Write(Html.DisplayFor(model => model.Parametros));

#line default
#line hidden
            EndContext();
            BeginContext(3084, 52, true);
            WriteLiteral("\r\n\t\t\t</dd>\r\n\t\t\t<dt>\r\n\t\t\t\tEstado\r\n\t\t\t</dt>\r\n\t\t\t<dd>\r\n");
            EndContext();
#line 118 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
                 if (Convert.ToBoolean(Model.Active))
				{

#line default
#line hidden
            BeginContext(3186, 25, true);
            WriteLiteral("\t\t\t\t\t<span>Ativo</span>\r\n");
            EndContext();
#line 121 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
				}
				else
				{

#line default
#line hidden
            BeginContext(3235, 27, true);
            WriteLiteral("\t\t\t\t\t<span>Inativo</span>\r\n");
            EndContext();
#line 125 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
				}

#line default
#line hidden
            BeginContext(3269, 63, true);
            WriteLiteral("\t\t\t</dd>\r\n\t\t\t<dt>\r\n\t\t\t\tData de Criação\r\n\t\t\t</dt>\r\n\t\t\t<dd>\r\n\t\t\t\t");
            EndContext();
            BeginContext(3333, 43, false);
#line 131 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
           Write(Html.DisplayFor(model => model.DataCriacao));

#line default
#line hidden
            EndContext();
            BeginContext(3376, 64, true);
            WriteLiteral("\r\n\t\t\t</dd>\r\n\t\t\t<dt>\r\n\t\t\t\tData de Edição\r\n\t\t\t</dt>\r\n\t\t\t<dd>\r\n\t\t\t\t");
            EndContext();
            BeginContext(3441, 42, false);
#line 137 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
           Write(Html.DisplayFor(model => model.DataEdicao));

#line default
#line hidden
            EndContext();
            BeginContext(3483, 60, true);
            WriteLiteral("\r\n\t\t\t</dd>\r\n\t\t\t<dt>\r\n\t\t\t\tCriado por\r\n\t\t\t</dt>\r\n\t\t\t<dd>\r\n\t\t\t\t");
            EndContext();
            BeginContext(3544, 43, false);
#line 143 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
           Write(Html.DisplayFor(model => model.UserCriacao));

#line default
#line hidden
            EndContext();
            BeginContext(3587, 61, true);
            WriteLiteral("\r\n\t\t\t</dd>\r\n\t\t\t<dt>\r\n\t\t\t\tEditado por\r\n\t\t\t</dt>\r\n\t\t\t<dd>\r\n\t\t\t\t");
            EndContext();
            BeginContext(3649, 42, false);
#line 149 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
           Write(Html.DisplayFor(model => model.UserEdicao));

#line default
#line hidden
            EndContext();
            BeginContext(3691, 23, true);
            WriteLiteral("\r\n\t\t\t</dd>\r\n\t\t</dl>\r\n\t\t");
            EndContext();
            BeginContext(3714, 300, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8ccdcd1a874d49db9bd546cac2a767b9", async() => {
                BeginContext(3740, 5, true);
                WriteLiteral("\r\n\t\t\t");
                EndContext();
                BeginContext(3745, 36, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "0e0a3d3c4b6048e78b7db2d64a8d6010", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
#line 153 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Delete.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Id);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3781, 152, true);
                WriteLiteral("\r\n\t\t\t<div class=\"btnSalvar\">\r\n\t\t\t\t<input type=\"submit\" value=\"Apagar\" class=\"btn btn-default\" id=\"save\"/>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"voltarLista\">\r\n\t\t\t\t");
                EndContext();
                BeginContext(3933, 59, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "de498fbd72f94a7fbd64920bc9f98614", async() => {
                    BeginContext(3971, 17, true);
                    WriteLiteral("Regressar à lista");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3992, 15, true);
                WriteLiteral("\r\n\t\t\t</div>\r\n\t\t");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(4014, 22, true);
            WriteLiteral("\r\n\t</div>\r\n\r\n\t</div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<DALCore.Models.Notificacoes> Html { get; private set; }
    }
}
#pragma warning restore 1591
