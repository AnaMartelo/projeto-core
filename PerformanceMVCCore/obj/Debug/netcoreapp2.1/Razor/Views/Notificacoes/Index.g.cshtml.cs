#pragma checksum "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7ced58c54d122fa58c80475173277109abbbe6fb"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Notificacoes_Index), @"mvc.1.0.view", @"/Views/Notificacoes/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Notificacoes/Index.cshtml", typeof(AspNetCore.Views_Notificacoes_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\_ViewImports.cshtml"
using PerformanceMVCCore;

#line default
#line hidden
#line 2 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\_ViewImports.cshtml"
using PerformanceMVCCore.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7ced58c54d122fa58c80475173277109abbbe6fb", @"/Views/Notificacoes/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"961db99ca7d6cb714de923693b3a57328cf0f52f", @"/Views/_ViewImports.cshtml")]
    public class Views_Notificacoes_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<DALCore.Models.Notificacoes>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Create", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("linkCriarNovo"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Details", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(49, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
  
    ViewData["Title"] = "Notificações";

#line default
#line hidden
            BeginContext(99, 364, true);
            WriteLiteral(@"<div id=""boxTableNotificacoes"">
	<div id=""headTituloView"">
		<h2 id=""h2titulo"">Notificações</h2>
	</div>
	<div id=""criarNovo"">
		<i class=""fas fa-user-edit""><link rel=""stylesheet"" href=""https://use.fontawesome.com/releases/v5.3.1/css/all.css"" integrity=""sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"" crossorigin=""anonymous""></i>
		");
            EndContext();
            BeginContext(463, 56, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a273e15471974edcb8559fde22023a30", async() => {
                BeginContext(505, 10, true);
                WriteLiteral("Criar Novo");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(519, 259, true);
            WriteLiteral(@"
	</div>
	<table class=""table"">
		<thead>
			<tr id=""tableHead"">
				<th>
					Designação
				</th>
				<th>
					Tipo de Notificação
				</th>
				<th>
					Tipo de Destinatário
				</th>
				<th>
					Destinatários
				</th>
				<th>
					");
            EndContext();
            BeginContext(779, 39, false);
#line 30 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
               Write(Html.DisplayNameFor(model => model.Dia));

#line default
#line hidden
            EndContext();
            BeginContext(818, 398, true);
            WriteLiteral(@"
				</th>
				<th>
					Assunto
				</th>
				<th>
					Texto
				</th>
				<th>
					Parâmetros
				</th>
				<th hidden>
					Estado
				</th>
				<th hidden>
					Data de Criação
				</th>
				<th hidden>
					Data de Edição
				</th>
				<th hidden>
					Criado por
				</th>
				<th hidden>
					Editado por
				</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
");
            EndContext();
#line 60 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
             foreach (var item in Model)
			{

#line default
#line hidden
            BeginContext(1255, 27, true);
            WriteLiteral("\t\t\t\t<tr>\r\n\t\t\t\t\t<td>\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(1283, 39, false);
#line 64 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Nome));

#line default
#line hidden
            EndContext();
            BeginContext(1322, 31, true);
            WriteLiteral("\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td>\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(1354, 50, false);
#line 67 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TipoNotificacao));

#line default
#line hidden
            EndContext();
            BeginContext(1404, 31, true);
            WriteLiteral("\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td>\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(1436, 51, false);
#line 70 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.TipoDestinatario));

#line default
#line hidden
            EndContext();
            BeginContext(1487, 31, true);
            WriteLiteral("\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td>\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(1519, 47, false);
#line 73 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Destinatario));

#line default
#line hidden
            EndContext();
            BeginContext(1566, 31, true);
            WriteLiteral("\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td>\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(1598, 38, false);
#line 76 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Dia));

#line default
#line hidden
            EndContext();
            BeginContext(1636, 31, true);
            WriteLiteral("\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td>\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(1668, 42, false);
#line 79 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Subject));

#line default
#line hidden
            EndContext();
            BeginContext(1710, 31, true);
            WriteLiteral("\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td>\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(1742, 39, false);
#line 82 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Body));

#line default
#line hidden
            EndContext();
            BeginContext(1781, 31, true);
            WriteLiteral("\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td>\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(1813, 45, false);
#line 85 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Parametros));

#line default
#line hidden
            EndContext();
            BeginContext(1858, 38, true);
            WriteLiteral("\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td hidden>\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(1897, 41, false);
#line 88 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Active));

#line default
#line hidden
            EndContext();
            BeginContext(1938, 38, true);
            WriteLiteral("\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td hidden>\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(1977, 46, false);
#line 91 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.DataCriacao));

#line default
#line hidden
            EndContext();
            BeginContext(2023, 38, true);
            WriteLiteral("\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td hidden>\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(2062, 45, false);
#line 94 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.DataEdicao));

#line default
#line hidden
            EndContext();
            BeginContext(2107, 38, true);
            WriteLiteral("\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td hidden>\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(2146, 46, false);
#line 97 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.UserCriacao));

#line default
#line hidden
            EndContext();
            BeginContext(2192, 38, true);
            WriteLiteral("\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td hidden>\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(2231, 45, false);
#line 100 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                   Write(Html.DisplayFor(modelItem => item.UserEdicao));

#line default
#line hidden
            EndContext();
            BeginContext(2276, 31, true);
            WriteLiteral("\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td>\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(2307, 55, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f26be8df07374be49f265124b6214a7e", async() => {
                BeginContext(2352, 6, true);
                WriteLiteral("Editar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 103 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                                               WriteLiteral(item.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2362, 10, true);
            WriteLiteral(" |\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(2372, 60, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a3602032d3fd4baf85a6a80436d28811", async() => {
                BeginContext(2420, 8, true);
                WriteLiteral("Detalhes");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 104 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                                                  WriteLiteral(item.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2432, 10, true);
            WriteLiteral(" |\r\n\t\t\t\t\t\t");
            EndContext();
            BeginContext(2442, 57, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "033d8e48349e4d73b4c0139b8c8e0f1d", async() => {
                BeginContext(2489, 6, true);
                WriteLiteral("Apagar");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 105 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
                                                 WriteLiteral(item.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2499, 25, true);
            WriteLiteral("\r\n\t\t\t\t\t</td>\r\n\t\t\t\t</tr>\r\n");
            EndContext();
#line 108 "C:\ACADEMIA\new project\PerformanceMVCCore\Views\Notificacoes\Index.cshtml"
			}

#line default
#line hidden
            BeginContext(2530, 29, true);
            WriteLiteral("\t\t</tbody>\r\n\t</table>\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<DALCore.Models.Notificacoes>> Html { get; private set; }
    }
}
#pragma warning restore 1591
