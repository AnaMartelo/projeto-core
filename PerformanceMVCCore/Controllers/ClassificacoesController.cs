﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DALCore.Models;

namespace PerformanceMVCCore.Controllers
{
    public class ClassificacoesController : Controller
    {
        private readonly PerformanceContext _context;

        public ClassificacoesController(PerformanceContext context)
        {
            _context = context;
        }

        // GET: Classificacoes
        public async Task<IActionResult> Index()
        {
			ViewBag.Current = "IndexClassificacoes";

			return View(await _context.Classificacoes.ToListAsync());
        }

        // GET: Classificacoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
			ViewBag.Current = "IndexClassificacoes";

			if (id == null)
            {
                return NotFound();
            }

            var classificacoes = await _context.Classificacoes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (classificacoes == null)
            {
                return NotFound();
            }

            return View(classificacoes);
        }

        // GET: Classificacoes/Create
        public IActionResult Create()
        {
			ViewBag.Current = "IndexClassificacoes";

			return View();
        }

        // POST: Classificacoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nota,Descricao,Ordem,Active,DataCriacao,DataEdicao,UserCriacao,UserEdicao")] Classificacoes classificacoes)
        {
            if (ModelState.IsValid)
            {
                //Para atualizar automaticamente para a data e hora atuais
                classificacoes.DataCriacao = DateTime.Now;
                classificacoes.DataEdicao = DateTime.Now;

                _context.Add(classificacoes);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(classificacoes);
        }

        // GET: Classificacoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
			ViewBag.Current = "IndexClassificacoes";

			if (id == null)
            {
                return NotFound();
            }

            var classificacoes = await _context.Classificacoes.FindAsync(id);
            if (classificacoes == null)
            {
                return NotFound();
            }
            return View(classificacoes);
        }

        // POST: Classificacoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nota,Descricao,Ordem,Active,DataCriacao,DataEdicao,UserCriacao,UserEdicao")] Classificacoes classificacoes)
        {
            if (id != classificacoes.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //Para atualizar automaticamente para a data e hora atuais
                    classificacoes.DataEdicao = DateTime.Now;

                    _context.Update(classificacoes);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClassificacoesExists(classificacoes.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(classificacoes);
        }

        // GET: Classificacoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Current = "IndexClassificacoes";

			if (id == null)
            {
                return NotFound();
            }

            var classificacoes = await _context.Classificacoes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (classificacoes == null)
            {
                return NotFound();
            }

            return View(classificacoes);
        }

        // POST: Classificacoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var classificacoes = await _context.Classificacoes.FindAsync(id);
            _context.Classificacoes.Remove(classificacoes);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ClassificacoesExists(int id)
        {
            return _context.Classificacoes.Any(e => e.Id == id);
        }
    }
}
