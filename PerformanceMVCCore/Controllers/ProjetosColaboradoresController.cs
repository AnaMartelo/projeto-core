﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DALCore.Models;

namespace PerformanceMVCCore.Controllers
{
    public class ProjetosColaboradoresController : Controller
    {
        private readonly PerformanceContext _context;

        public ProjetosColaboradoresController(PerformanceContext context)
        {
            _context = context;
        }

        // GET: ProjetosColaboradores
        public async Task<IActionResult> Index()
        {
            var performanceContext = _context.ProjetosColaboradores.Include(p => p.IdColaboradorNavigation).Include(p => p.IdProjetoNavigation);
            return View(await performanceContext.ToListAsync());
        }

        // GET: ProjetosColaboradores/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var projetosColaboradores = await _context.ProjetosColaboradores
                .Include(p => p.IdColaboradorNavigation)
                .Include(p => p.IdProjetoNavigation)
                .FirstOrDefaultAsync(m => m.IdProjeto == id);
            if (projetosColaboradores == null)
            {
                return NotFound();
            }

            return View(projetosColaboradores);
        }

        // GET: ProjetosColaboradores/Create
        public IActionResult Create()
        {
            ViewData["IdColaborador"] = new SelectList(_context.Colaboradores, "Id", "Id");
            ViewData["IdProjeto"] = new SelectList(_context.Projetos, "Id", "Nome");
            return View();
        }

        // POST: ProjetosColaboradores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdProjeto,IdColaborador")] ProjetosColaboradores projetosColaboradores)
        {
            if (ModelState.IsValid)
            {
                _context.Add(projetosColaboradores);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdColaborador"] = new SelectList(_context.Colaboradores, "Id", "Id", projetosColaboradores.IdColaborador);
            ViewData["IdProjeto"] = new SelectList(_context.Projetos, "Id", "Nome", projetosColaboradores.IdProjeto);
            return View(projetosColaboradores);
        }

        // GET: ProjetosColaboradores/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var projetosColaboradores = await _context.ProjetosColaboradores.FindAsync(id);
            if (projetosColaboradores == null)
            {
                return NotFound();
            }
            ViewData["IdColaborador"] = new SelectList(_context.Colaboradores, "Id", "Id", projetosColaboradores.IdColaborador);
            ViewData["IdProjeto"] = new SelectList(_context.Projetos, "Id", "Nome", projetosColaboradores.IdProjeto);
            return View(projetosColaboradores);
        }

        // POST: ProjetosColaboradores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdProjeto,IdColaborador")] ProjetosColaboradores projetosColaboradores)
        {
            if (id != projetosColaboradores.IdProjeto)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(projetosColaboradores);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjetosColaboradoresExists(projetosColaboradores.IdProjeto))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdColaborador"] = new SelectList(_context.Colaboradores, "Id", "Id", projetosColaboradores.IdColaborador);
            ViewData["IdProjeto"] = new SelectList(_context.Projetos, "Id", "Nome", projetosColaboradores.IdProjeto);
            return View(projetosColaboradores);
        }

        // GET: ProjetosColaboradores/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var projetosColaboradores = await _context.ProjetosColaboradores
                .Include(p => p.IdColaboradorNavigation)
                .Include(p => p.IdProjetoNavigation)
                .FirstOrDefaultAsync(m => m.IdProjeto == id);
            if (projetosColaboradores == null)
            {
                return NotFound();
            }

            return View(projetosColaboradores);
        }

        // POST: ProjetosColaboradores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var projetosColaboradores = await _context.ProjetosColaboradores.FindAsync(id);
            _context.ProjetosColaboradores.Remove(projetosColaboradores);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProjetosColaboradoresExists(int id)
        {
            return _context.ProjetosColaboradores.Any(e => e.IdProjeto == id);
        }
    }
}
