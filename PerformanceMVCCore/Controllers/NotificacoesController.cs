﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DALCore.Models;

namespace PerformanceMVCCore.Controllers
{
    public class NotificacoesController : Controller
    {
        private readonly PerformanceContext _context;

        public NotificacoesController(PerformanceContext context)
        {
            _context = context;
        }

        // GET: Notificacoes
        public async Task<IActionResult> Index()
        {
            return View(await _context.Notificacoes.ToListAsync());
        }

        // GET: Notificacoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificacoes = await _context.Notificacoes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (notificacoes == null)
            {
                return NotFound();
            }

            return View(notificacoes);
        }

        // GET: Notificacoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Notificacoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nome,TipoNotificacao,TipoDestinatario,Destinatario,Dia,Subject,Body,Parametros,Active,DataCriacao,DataEdicao,UserCriacao,UserEdicao")] Notificacoes notificacoes)
        {
            if (ModelState.IsValid)
            {
                //Para atualizar automaticamente para a data e hora atuais
                notificacoes.DataCriacao = DateTime.Now;
                notificacoes.DataEdicao = DateTime.Now;

                _context.Add(notificacoes);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(notificacoes);
        }

        // GET: Notificacoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificacoes = await _context.Notificacoes.FindAsync(id);
            if (notificacoes == null)
            {
                return NotFound();
            }
            return View(notificacoes);
        }

        // POST: Notificacoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nome,TipoNotificacao,TipoDestinatario,Destinatario,Dia,Subject,Body,Parametros,Active,DataCriacao,DataEdicao,UserCriacao,UserEdicao")] Notificacoes notificacoes)
        {
            if (id != notificacoes.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //Para atualizar automaticamente para a data e hora atuais
                    notificacoes.DataEdicao = DateTime.Now;

                    _context.Update(notificacoes);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NotificacoesExists(notificacoes.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(notificacoes);
        }

        // GET: Notificacoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificacoes = await _context.Notificacoes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (notificacoes == null)
            {
                return NotFound();
            }

            return View(notificacoes);
        }

        // POST: Notificacoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var notificacoes = await _context.Notificacoes.FindAsync(id);
            _context.Notificacoes.Remove(notificacoes);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NotificacoesExists(int id)
        {
            return _context.Notificacoes.Any(e => e.Id == id);
        }
    }
}
