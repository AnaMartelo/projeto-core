﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DALCore.Models;
using Microsoft.AspNetCore.Http;

namespace PerformanceMVCCore.Controllers
{
    public class AvaliacoesController : Controller
    {
        private readonly PerformanceContext _context;

        public AvaliacoesController(PerformanceContext context)
        {
            _context = context;
        }

        // GET: Avaliacoes
        public async Task<IActionResult> Index()
        {
			ViewBag.Current = "IndexAvaliacoes";

			var performanceContext = _context.Avaliacoes.Include(a => a.IdAvaliadorNavigation).Include(a => a.IdClassificacaoNavigation).Include(a => a.IdProjetoNavigation).Include(a => a.IdUtilizadorNavigation);
            return View(await performanceContext.ToListAsync());
        }

        // GET: Avaliacoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
			ViewBag.Current = "IndexAvaliacoes";

			if (id == null)
            {
                return NotFound();
            }

            var avaliacoes = await _context.Avaliacoes
                .Include(a => a.IdAvaliadorNavigation)
                .Include(a => a.IdClassificacaoNavigation)
                .Include(a => a.IdProjetoNavigation)
                .Include(a => a.IdUtilizadorNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (avaliacoes == null)
            {
                return NotFound();
            }

            return View(avaliacoes);
        }

        // GET: Avaliacoes/Create
        public IActionResult Create()
        {
			ViewBag.Current = "IndexAvaliacoes";

			ViewData["IdAvaliador"] = new SelectList(_context.Utilizadores, "Id", "Nome");
            ViewData["IdClassificacao"] = new SelectList(_context.Classificacoes, "Id", "Nota");
            ViewData["IdProjeto"] = new SelectList(_context.Projetos, "Id", "Nome");
			ViewData["AvColaborador"] = new SelectList(_context.Utilizadores.Where(x => x.IdFuncao == 4), "Id", "Nome");
			ViewData["AvGestor"] = new SelectList(_context.Utilizadores.Where(x => x.IdFuncao == 5), "Id", "Nome");
			return View();
        }

        // POST: Avaliacoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdClassificacao,IdUtilizador,IdAvaliador,IdProjeto,Estado,Mes,Ano,PontosFortes,PontosFracos,Active,DataCriacao,DataEdicao")] Avaliacoes avaliacoes, string avfuncao, int colaboradorAv, int gestorAv)
		{

			if (ModelState.IsValid)
            {
				// Passa o valor submetida ao estado da avaliação
				avaliacoes.Estado = "Submetida";

				//Passa o Id da pessoa que está logada ao IdAvaliador (o avaliador fica automático, ou seja, o avaliador é a pessoa que fez login)
				String User = HttpContext.Session.GetString("MySession");
				avaliacoes.IdAvaliador = _context.Utilizadores.Where(x => x.Username == User).FirstOrDefault().Id;

				//Passa o valor para o IdUtilizador (pessoa avaliada). Filtra os utilizadores que vão ser avaliados dos que não vão
				if (avfuncao == "Colaborador")
				{
					avaliacoes.IdUtilizador = colaboradorAv;
				}
				if (avfuncao == "Gestor de Projeto")
				{
					avaliacoes.IdUtilizador = gestorAv;
				}
				

				//Para atualizar automaticamente para a data e hora atuais
				avaliacoes.DataCriacao = DateTime.Now;
                avaliacoes.DataEdicao = DateTime.Now;

                _context.Add(avaliacoes);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdAvaliador"] = new SelectList(_context.Utilizadores, "Id", "Nome", avaliacoes.IdAvaliador);
            ViewData["IdClassificacao"] = new SelectList(_context.Classificacoes, "Id", "Nota", avaliacoes.IdClassificacao);
            ViewData["IdProjeto"] = new SelectList(_context.Projetos, "Id", "Nome", avaliacoes.IdProjeto);
            ViewData["IdUtilizador"] = new SelectList(_context.Utilizadores, "Id", "Nome", avaliacoes.IdUtilizador);
            return View(avaliacoes);
        }

        // GET: Avaliacoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
			ViewBag.Current = "IndexAvaliacoes";

			if (id == null)
            {
                return NotFound();
            }

            var avaliacoes = await _context.Avaliacoes.FindAsync(id);
            if (avaliacoes == null)
            {
                return NotFound();
            }
            ViewData["IdAvaliador"] = new SelectList(_context.Utilizadores, "Id", "Nome", avaliacoes.IdAvaliador);
            ViewData["IdClassificacao"] = new SelectList(_context.Classificacoes, "Id", "Nota", avaliacoes.IdClassificacao);
            ViewData["IdProjeto"] = new SelectList(_context.Projetos, "Id", "Nome", avaliacoes.IdProjeto);
            ViewData["IdUtilizador"] = new SelectList(_context.Utilizadores, "Id", "Nome", avaliacoes.IdUtilizador);
            return View(avaliacoes);
        }

        // POST: Avaliacoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdClassificacao,IdUtilizador,IdAvaliador,IdProjeto,Estado,Mes,Ano,PontosFortes,PontosFracos,Active,DataCriacao,DataEdicao")] Avaliacoes avaliacoes)
        {
            if (id != avaliacoes.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //Para atualizar automaticamente para a data e hora atuais
                    avaliacoes.DataEdicao = DateTime.Now;

                    _context.Update(avaliacoes);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AvaliacoesExists(avaliacoes.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdAvaliador"] = new SelectList(_context.Utilizadores, "Id", "Nome", avaliacoes.IdAvaliador);
            ViewData["IdClassificacao"] = new SelectList(_context.Classificacoes, "Id", "Nota", avaliacoes.IdClassificacao);
            ViewData["IdProjeto"] = new SelectList(_context.Projetos, "Id", "Nome", avaliacoes.IdProjeto);
            ViewData["IdUtilizador"] = new SelectList(_context.Utilizadores, "Id", "Nome", avaliacoes.IdUtilizador);
            return View(avaliacoes);
        }

        // GET: Avaliacoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Current = "IndexAvaliacoes";

			if (id == null)
            {
                return NotFound();
            }

            var avaliacoes = await _context.Avaliacoes
                .Include(a => a.IdAvaliadorNavigation)
                .Include(a => a.IdClassificacaoNavigation)
                .Include(a => a.IdProjetoNavigation)
                .Include(a => a.IdUtilizadorNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (avaliacoes == null)
            {
                return NotFound();
            }

            return View(avaliacoes);
        }

        // POST: Avaliacoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var avaliacoes = await _context.Avaliacoes.FindAsync(id);
            _context.Avaliacoes.Remove(avaliacoes);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AvaliacoesExists(int id)
        {
            return _context.Avaliacoes.Any(e => e.Id == id);
        }
    }
}
