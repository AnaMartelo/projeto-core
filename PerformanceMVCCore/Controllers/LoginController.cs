﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DALCore.Models;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore.SqlServer.Storage.Internal;
using Microsoft.EntityFrameworkCore;
using System.Data;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;


namespace PeformanceMVCCore.Controllers
{
    [Route("Login")]
    public class LoginController : Controller
    {
        private readonly PerformanceContext _context;
        public LoginController(PerformanceContext context)
        {
            _context = context;
        }
        public string Password { get; private set; }
        public string Username { get; private set; }

        [Route("")]
        [Route("index")]
        [Route("~/")]
        public IActionResult Index()
        {
            return View();
        }

		[HttpPost]
		public IActionResult Login(Utilizadores u)
		{

			var utilizador = _context.Utilizadores.FirstOrDefault(x => x.Username == u.Username);

			if (utilizador != null)
			{
				if ((u.Username != null || u.Password != null) && (u.Username.Equals(utilizador.Username) && u.Password.Equals(utilizador.Password)))
				{
					//rota do Admin
					if (utilizador.IdFuncao == 1)
					{
						HttpContext.Session.SetString("MySession", u.Username);
						return View("~/Views/Home/Index.cshtml");
					}
					//rota do manager
					if (utilizador.IdFuncao == 2)
					{
						HttpContext.Session.SetString("MySession", u.Username);
						return View("~/Views/Home/Index.cshtml");
					}
					//rota do cliente
					if (utilizador.IdFuncao == 3)
					{
						HttpContext.Session.SetString("MySession", u.Username);
						return View("~/Views/Avaliacoes/Create.cshtml");
					}
					//rota do colaborador
					if (utilizador.IdFuncao == 4)
					{
						HttpContext.Session.SetString("MySession", u.Username);
						return View("~/Views/Shared/WorkInProgress.cshtml");
					}
					//rota do gestor de projeto
					if (utilizador.IdFuncao == 5)
					{
						HttpContext.Session.SetString("MySession", u.Username);
						return View("~/Views/Home/Index.cshtml");
					}

					else
					{
						return View("~/Views/teste/SemAutorizacao.cshtml");
					}
				}
				else
				{
					ViewBag.error = "Password incorreta";
					return View("Index");
				}

			}
			if (utilizador == null)
			{
				ViewBag.error = "Nome de utilizador não existe";
				return View("Index");
			}
				return View("Index");
		}
		
		[HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("MySession");
			HttpContext.Session.Clear();
			Response.Cookies.Delete("MySession");
			return RedirectToAction("Index", "Login");
        }

	}

}
