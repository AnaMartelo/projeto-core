﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DALCore.Models;

namespace PerformanceMVCCore.Controllers
{
    public class NotificacoesDesativarsController : Controller
    {
        private readonly PerformanceContext _context;

        public NotificacoesDesativarsController(PerformanceContext context)
        {
            _context = context;
        }

        // GET: NotificacoesDesativars
        public async Task<IActionResult> Index()
        {
            var performanceContext = _context.NotificacoesDesativar.Include(n => n.IdNotificacaoNavigation).Include(n => n.IdUtilizadorNavigation);
            return View(await performanceContext.ToListAsync());
        }

        // GET: NotificacoesDesativars/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificacoesDesativar = await _context.NotificacoesDesativar
                .Include(n => n.IdNotificacaoNavigation)
                .Include(n => n.IdUtilizadorNavigation)
                .FirstOrDefaultAsync(m => m.IdUtilizador == id);
            if (notificacoesDesativar == null)
            {
                return NotFound();
            }

            return View(notificacoesDesativar);
        }

        // GET: NotificacoesDesativars/Create
        public IActionResult Create()
        {
            ViewData["IdNotificacao"] = new SelectList(_context.Notificacoes, "Id", "Body");
            ViewData["IdUtilizador"] = new SelectList(_context.Utilizadores, "Id", "Nome");
            return View();
        }

        // POST: NotificacoesDesativars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdUtilizador,IdNotificacao,DataDesativacao")] NotificacoesDesativar notificacoesDesativar)
        {
            if (ModelState.IsValid)
            {
				notificacoesDesativar.DataDesativacao = DateTime.Now;

                _context.Add(notificacoesDesativar);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdNotificacao"] = new SelectList(_context.Notificacoes, "Id", "Body", notificacoesDesativar.IdNotificacao);
            ViewData["IdUtilizador"] = new SelectList(_context.Utilizadores, "Id", "Nome", notificacoesDesativar.IdUtilizador);
            return View(notificacoesDesativar);
        }

        // GET: NotificacoesDesativars/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificacoesDesativar = await _context.NotificacoesDesativar.FindAsync(id);
            if (notificacoesDesativar == null)
            {
                return NotFound();
            }
            ViewData["IdNotificacao"] = new SelectList(_context.Notificacoes, "Id", "Body", notificacoesDesativar.IdNotificacao);
            ViewData["IdUtilizador"] = new SelectList(_context.Utilizadores, "Id", "Nome", notificacoesDesativar.IdUtilizador);
            return View(notificacoesDesativar);
        }

        // POST: NotificacoesDesativars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdUtilizador,IdNotificacao,DataDesativacao")] NotificacoesDesativar notificacoesDesativar)
        {
            if (id != notificacoesDesativar.IdUtilizador)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(notificacoesDesativar);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NotificacoesDesativarExists(notificacoesDesativar.IdUtilizador))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdNotificacao"] = new SelectList(_context.Notificacoes, "Id", "Body", notificacoesDesativar.IdNotificacao);
            ViewData["IdUtilizador"] = new SelectList(_context.Utilizadores, "Id", "Nome", notificacoesDesativar.IdUtilizador);
            return View(notificacoesDesativar);
        }

        // GET: NotificacoesDesativars/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificacoesDesativar = await _context.NotificacoesDesativar
                .Include(n => n.IdNotificacaoNavigation)
                .Include(n => n.IdUtilizadorNavigation)
                .FirstOrDefaultAsync(m => m.IdUtilizador == id);
            if (notificacoesDesativar == null)
            {
                return NotFound();
            }

            return View(notificacoesDesativar);
        }

        // POST: NotificacoesDesativars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var notificacoesDesativar = await _context.NotificacoesDesativar.FindAsync(id);
            _context.NotificacoesDesativar.Remove(notificacoesDesativar);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NotificacoesDesativarExists(int id)
        {
            return _context.NotificacoesDesativar.Any(e => e.IdUtilizador == id);
        }
    }
}
