﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace PerformanceMVCCore.Controllers
{
	public class DashboardsController : Controller
	{
		public IActionResult Index()
		{
			ViewBag.Current = "IndexDashboards";

			return View("~/Views/Dashboards/Index.cshtml");
		}
	}
}
