﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DALCore.Models;

namespace PerformanceMVCCore.Controllers
{
    public class FuncoesController : Controller
    {
        private readonly PerformanceContext _context;

        public FuncoesController(PerformanceContext context)
        {
            _context = context;
        }

        // GET: Funcoes
        public async Task<IActionResult> Index()
        {
            return View(await _context.Funcoes.ToListAsync());
        }

        // GET: Funcoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var funcoes = await _context.Funcoes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (funcoes == null)
            {
                return NotFound();
            }

            return View(funcoes);
        }

        // GET: Funcoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Funcoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nome,Active,DataCriacao,DataEdicao,UserCriacao,UserEdicao")] Funcoes funcoes)
        {
            if (ModelState.IsValid)
            {
                //Para atualizar automaticamente para a data e hora atuais
                funcoes.DataCriacao = DateTime.Now;
                funcoes.DataEdicao = DateTime.Now;

                _context.Add(funcoes);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(funcoes);
        }

        // GET: Funcoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var funcoes = await _context.Funcoes.FindAsync(id);
            if (funcoes == null)
            {
                return NotFound();
            }
            return View(funcoes);
        }

        // POST: Funcoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nome,Active,DataCriacao,DataEdicao,UserCriacao,UserEdicao")] Funcoes funcoes)
        {
            if (id != funcoes.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //Para atualizar automaticamente para a data e hora atuais
                    funcoes.DataEdicao = DateTime.Now;

                    _context.Update(funcoes);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FuncoesExists(funcoes.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(funcoes);
        }

        // GET: Funcoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var funcoes = await _context.Funcoes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (funcoes == null)
            {
                return NotFound();
            }

            return View(funcoes);
        }

        // POST: Funcoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var funcoes = await _context.Funcoes.FindAsync(id);
            _context.Funcoes.Remove(funcoes);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FuncoesExists(int id)
        {
            return _context.Funcoes.Any(e => e.Id == id);
        }
    }
}
