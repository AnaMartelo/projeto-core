﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DALCore.Models;

namespace PerformanceMVCCore.Controllers
{
    public class ProjetosController : Controller
    {

        private readonly PerformanceContext _context;

        public ProjetosController(PerformanceContext context)
        {
            _context = context;
        }

        // GET: Projetos
        public async Task<IActionResult> Index()
        {
			ViewBag.Current = "IndexProjetos";

			var performanceContext = _context.Projetos.Include(p => p.IdClienteNavigation).Include(p => p.IdGestorNavigation);
            return View(await performanceContext.ToListAsync());
        }

        // GET: Projetos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
			ViewBag.Current = "IndexProjetos";

			if (id == null)
            {
                return NotFound();
            }

            var projetos = await _context.Projetos
                .Include(p => p.IdClienteNavigation)
                .Include(p => p.IdGestorNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (projetos == null)
            {
                return NotFound();
            }

            return View(projetos);
        }

        // GET: Projetos/Create
        public IActionResult Create()
        {
			ViewBag.Current = "IndexProjetos";
			ViewBag.IdColaborador = new SelectList(_context.Utilizadores.Where(x => x.IdFuncao == 4), "Id", "Nome");

			ViewData["IdCliente"] = new SelectList(_context.Clientes, "Id", "Empresa");
            ViewData["IdGestor"] = new SelectList(_context.Utilizadores.Where(x=> x.IdFuncao == 5), "Id", "Nome");
            return View();
        }

        // POST: Projetos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdCliente,IdGestor,Nome,Active,DataCriacao,DataEdicao,UserCriacao,UserEdicao")] Projetos projetos, string ListaColaboradores)
        {

			if (ModelState.IsValid)
            {
                //Para atualizar automaticamente para a data e hora atuais
                projetos.DataCriacao = DateTime.Now;
                projetos.DataEdicao = DateTime.Now;

                _context.Add(projetos);
                await _context.SaveChangesAsync();

				//adicionar a lista de colaboradores à tabela ProjetosColaboradores
				AddColaboradores(projetos.Id, ListaColaboradores);

				return RedirectToAction(nameof(Index));
            }
            ViewData["IdCliente"] = new SelectList(_context.Clientes, "Id", "Empresa", projetos.IdCliente);
            ViewData["IdGestor"] = new SelectList(_context.Utilizadores, "Id", "Nome", projetos.IdGestor);
            return View(projetos);
        }

		//adicionar colaborador a projeto - Processo
		public bool AddColaboradores(int Id, string ListaColaboradores)
		{
			List<string> colaboradores = ListaColaboradores.Split(',').ToList();

			for (int i = 0; i < colaboradores.Count(); i++)
			{
				if (colaboradores[i] != "")
				{
					ProjetosColaboradores novoColaborador = new ProjetosColaboradores();
					novoColaborador.IdProjeto = Id;
					novoColaborador.IdColaborador = Convert.ToInt32(colaboradores[i]);

					_context.ProjetosColaboradores.Add(novoColaborador);
					_context.SaveChanges();
				}
			}
			return true;
		}
		//FIM adicionar colaborador a projeto - Processo

		// GET: Projetos/Edit/5
		public async Task<IActionResult> Edit(int? id)
        {
			ViewBag.Current = "IndexProjetos";

			if (id == null)
            {
                return NotFound();
            }

            var projetos = await _context.Projetos.FindAsync(id);
            if (projetos == null)
            {
                return NotFound();
            }
            ViewData["IdCliente"] = new SelectList(_context.Clientes, "Id", "Empresa", projetos.IdCliente);
            ViewData["IdGestor"] = new SelectList(_context.Utilizadores, "Id", "Nome", projetos.IdGestor);
            return View(projetos);
        }

        // POST: Projetos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdCliente,IdGestor,Nome,Active,DataCriacao,DataEdicao,UserCriacao,UserEdicao")] Projetos projetos)
        {

			if (id != projetos.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //Para atualizar automaticamente para a data e hora atuais
                    projetos.DataEdicao = DateTime.Now;

                    _context.Update(projetos);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjetosExists(projetos.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCliente"] = new SelectList(_context.Clientes, "Id", "Empresa", projetos.IdCliente);
            ViewData["IdGestor"] = new SelectList(_context.Utilizadores, "Id", "Nome", projetos.IdGestor);
            return View(projetos);
        }

        // GET: Projetos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Current = "IndexProjetos";

			if (id == null)
            {
                return NotFound();
            }

            var projetos = await _context.Projetos
                .Include(p => p.IdClienteNavigation)
                .Include(p => p.IdGestorNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (projetos == null)
            {
                return NotFound();
            }

            return View(projetos);
        }

        // POST: Projetos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {

			var projetos = await _context.Projetos.FindAsync(id);
            _context.Projetos.Remove(projetos);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProjetosExists(int id)
        {
            return _context.Projetos.Any(e => e.Id == id);
        }
    }
}
