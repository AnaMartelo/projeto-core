﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DALCore.Models;

namespace PerformanceMVCCore.Controllers
{
    public class UtilizadoresController : Controller
    {
        private readonly PerformanceContext _context;

        public UtilizadoresController(PerformanceContext context)
        {
            _context = context;
        }

        // GET: Utilizadores
        public async Task<IActionResult> Index()
        {
			ViewBag.Current = "IndexUtilizadores";

			var performanceContext = _context.Utilizadores.Include(u => u.IdFuncaoNavigation);
            return View(await performanceContext.ToListAsync());
        }

        // GET: Utilizadores/Details/5
        public async Task<IActionResult> Details(int? id)
        {
			ViewBag.Current = "IndexUtilizadores";

			if (id == null)
            {
                return NotFound();
            }

            var utilizadores = await _context.Utilizadores
                .Include(u => u.IdFuncaoNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (utilizadores == null)
            {
                return NotFound();
            }

            return View(utilizadores);
        }

        // GET: Utilizadores/Create
        public IActionResult Create()
        {
			ViewBag.Current = "IndexUtilizadores";

			//ViewBag.IdFuncao 
			ViewData["IdFuncao"] = new SelectList(_context.Funcoes, "Id", "Nome");
			//ViewBag.Nome    passa o Nomo do utilizador que tem id == idmanager 
			ViewData["Nome"] = new SelectList(_context.Utilizadores.Where(x => x.IdFuncao == 2), "Id", "Nome");

			return View();
		}

        // POST: Utilizadores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdFuncao,Username,Password,Nome,Email,Telefone,Active,DataCriacao,DataEdicao,UserCriacao,UserEdicao")] Utilizadores utilizadores, string nif, string empresa, int idmanager)
        {
            if (ModelState.IsValid)
            {
                //Para não permitir criação em duplicado de users e emails
                if (_context.Utilizadores.Where(x=> x.Username == utilizadores.Username).Any())
                    return Content("Username já existe.");
                if (_context.Utilizadores.Where(x => x.Username == utilizadores.Email).Any())
                    return Content("O email já existe");

				// Definindo nif e empresa da partial view (utilizadores/cliente)
				if (utilizadores.IdFuncao == _context.Funcoes.Where(x => x.Nome == "Cliente").FirstOrDefault().Id)
				{
					Clientes cliente = new Clientes();
					cliente.Nif = nif;
					cliente.Empresa = empresa;

					utilizadores.Clientes = cliente;
				}

				//Definindo manager do colaborador da partial view (utilizadores/colaborador)
				if (utilizadores.IdFuncao == _context.Funcoes.Where(x => x.Nome == "Colaborador").FirstOrDefault().Id)
				{
					Colaboradores colaborador = new Colaboradores();
					colaborador.IdManager = idmanager;

					utilizadores.Colaboradores = colaborador;
				}

				//Para atualizar automaticamente para a data e hora atuais
				utilizadores.DataCriacao = DateTime.Now;
                utilizadores.DataEdicao = DateTime.Now;
                                
                _context.Add(utilizadores);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdFuncao"] = new SelectList(_context.Funcoes, "Id", "Nome", utilizadores.IdFuncao);
            return View(utilizadores);
        }

        // GET: Utilizadores/Edit/5
        public async Task<IActionResult> Edit(int? id, string nifedit, string empresaedit, int idmanageredit)
        {
			ViewBag.Current = "IndexUtilizadores";

			if (id == null)
            {
                return NotFound();
            }

            var utilizadores = await _context.Utilizadores.FindAsync(id);

			//GET clientes para mostrar na view edit
			if (utilizadores.IdFuncao == _context.Funcoes.Where(x => x.Nome == "Cliente").FirstOrDefault().Id)
			{
				var clientesedit = _context.Clientes.Find(id);
				utilizadores.Clientes = clientesedit;
			}
			//GET colaboradores para mostrar na view edit
			if (utilizadores.IdFuncao == _context.Funcoes.Where(x => x.Nome == "Colaborador").FirstOrDefault().Id)
			{
				var colaboradoresedit = _context.Colaboradores.Find(id);
				utilizadores.Colaboradores = colaboradoresedit;
			}


			if (utilizadores == null)
            {
                return NotFound();
            }

            ViewData["IdFuncao"] = new SelectList(_context.Funcoes, "Id", "Nome", utilizadores.IdFuncao);
			ViewData["Nome"] = new SelectList(_context.Utilizadores.Where(x => x.IdFuncao == 2), "Id", "Nome");
			return View(utilizadores);
        }

        // POST: Utilizadores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdFuncao,Username,Password,Nome,Email,Telefone,Active,DataCriacao,DataEdicao,UserCriacao,UserEdicao")] Utilizadores utilizadores, Clientes clientes, Colaboradores colaborador)
        {
            if (id != utilizadores.Id)
            {
				ViewBag.Current = "IndexUtilizadores";

				return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //Para atualizar automaticamente para a data e hora atuais
                    utilizadores.DataEdicao = DateTime.Now;
                    
                    _context.Update(utilizadores);

					//Editar clientes
					if (utilizadores.IdFuncao == _context.Funcoes.Where(x => x.Nome == "Cliente").FirstOrDefault().Id)
					{
						_context.Update(clientes);

					}
					//Editar colaboradores
					if (utilizadores.IdFuncao == _context.Funcoes.Where(x => x.Nome == "Colaborador").FirstOrDefault().Id)
					{
						_context.Update(colaborador);

					}
						await _context.SaveChangesAsync();
					
				}
                catch (DbUpdateConcurrencyException)
                {
                    if (!UtilizadoresExists(utilizadores.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdFuncao"] = new SelectList(_context.Funcoes, "Id", "Nome", utilizadores.IdFuncao);
            return View(utilizadores);
        }

        // GET: Utilizadores/Delete/5
        public async Task<IActionResult> Delete(int? id, string nif, string empresa, int idmanager)
        {
			ViewBag.Current = "IndexUtilizadores";

			if (id == null)
            {
                return NotFound();
            }

            var utilizadores = await _context.Utilizadores
                .Include(u => u.IdFuncaoNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);



			if (utilizadores == null)
			{
				return NotFound();
			}
			return View(utilizadores);
        }

        // POST: Utilizadores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
			ViewBag.Current = "IndexUtilizadores";

			var utilizadores = await _context.Utilizadores.FindAsync(id);

			//deletar clientes  e colaboradores antes de utilizadores

			if (utilizadores.IdFuncao == _context.Funcoes.Where(x => x.Nome == "Cliente").FirstOrDefault().Id)
			{
				var clientes = await _context.Clientes.FindAsync(id);
				_context.Clientes.Remove(clientes);
			}

			if (utilizadores.IdFuncao == _context.Funcoes.Where(x => x.Nome == "Colaborador").FirstOrDefault().Id)
			{
				var colaborador = await _context.Colaboradores.FindAsync(id);
				_context.Colaboradores.Remove(colaborador);
			}


            _context.Utilizadores.Remove(utilizadores);

			await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));			

		}

        private bool UtilizadoresExists(int id)
        {
            return _context.Utilizadores.Any(e => e.Id == id);
        }
    }
}
