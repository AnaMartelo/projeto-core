﻿using System;
using System.Collections.Generic;

namespace DALCore.Models
{
    public partial class Notificacoes
    {
        public Notificacoes()
        {
            NotificacoesDesativar = new HashSet<NotificacoesDesativar>();
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string TipoNotificacao { get; set; }
        public string TipoDestinatario { get; set; }
        public string Destinatario { get; set; }
        public int? Dia { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Parametros { get; set; }
        public bool? Active { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataEdicao { get; set; }
        public string UserCriacao { get; set; }
        public string UserEdicao { get; set; }

        public ICollection<NotificacoesDesativar> NotificacoesDesativar { get; set; }
    }
}
