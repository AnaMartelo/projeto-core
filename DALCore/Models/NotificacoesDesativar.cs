﻿using System;
using System.Collections.Generic;

namespace DALCore.Models
{
    public partial class NotificacoesDesativar
    {
        public int IdUtilizador { get; set; }
        public int IdNotificacao { get; set; }
        public DateTime? DataDesativacao { get; set; }

        public Notificacoes IdNotificacaoNavigation { get; set; }
        public Utilizadores IdUtilizadorNavigation { get; set; }
    }
}
