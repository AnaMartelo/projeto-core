﻿using System;
using System.Collections.Generic;

namespace DALCore.Models
{
    public partial class Classificacoes
    {
        public Classificacoes()
        {
            Avaliacoes = new HashSet<Avaliacoes>();
        }

        public int Id { get; set; }
        public string Nota { get; set; }
        public string Descricao { get; set; }
        public int Ordem { get; set; }
        public bool? Active { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataEdicao { get; set; }
        public string UserCriacao { get; set; }
        public string UserEdicao { get; set; }

        public ICollection<Avaliacoes> Avaliacoes { get; set; }
    }
}
