﻿using System;
using System.Collections.Generic;

namespace DALCore.Models
{
    public partial class Projetos
    {
        public Projetos()
        {
            Avaliacoes = new HashSet<Avaliacoes>();
            ProjetosColaboradores = new HashSet<ProjetosColaboradores>();
        }

        public int Id { get; set; }
        public int IdCliente { get; set; }
        public int IdGestor { get; set; }
        public string Nome { get; set; }
        public bool? Active { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataEdicao { get; set; }
        public string UserCriacao { get; set; }
        public string UserEdicao { get; set; }

        public Clientes IdClienteNavigation { get; set; }
        public Utilizadores IdGestorNavigation { get; set; }
        public ICollection<Avaliacoes> Avaliacoes { get; set; }
        public ICollection<ProjetosColaboradores> ProjetosColaboradores { get; set; }
    }
}
