﻿using System;
using System.Collections.Generic;

namespace DALCore.Models
{
    public partial class Clientes
    {
        public Clientes()
        {
            Projetos = new HashSet<Projetos>();
        }

        public int Id { get; set; }
        public string Nif { get; set; }
        public string Empresa { get; set; }

        public Utilizadores IdNavigation { get; set; }
        public ICollection<Projetos> Projetos { get; set; }
    }
}
