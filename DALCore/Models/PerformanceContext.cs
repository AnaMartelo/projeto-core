﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DALCore.Models
{
    public partial class PerformanceContext : DbContext
    {
        public PerformanceContext()
        {
        }

        public PerformanceContext(DbContextOptions<PerformanceContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Avaliacoes> Avaliacoes { get; set; }
        public virtual DbSet<Classificacoes> Classificacoes { get; set; }
        public virtual DbSet<Clientes> Clientes { get; set; }
        public virtual DbSet<Colaboradores> Colaboradores { get; set; }
        public virtual DbSet<Funcoes> Funcoes { get; set; }
        public virtual DbSet<Notificacoes> Notificacoes { get; set; }
        public virtual DbSet<NotificacoesDesativar> NotificacoesDesativar { get; set; }
        public virtual DbSet<Projetos> Projetos { get; set; }
        public virtual DbSet<ProjetosColaboradores> ProjetosColaboradores { get; set; }
        public virtual DbSet<Utilizadores> Utilizadores { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=PC-0042\\SQLEXPRESS;Database=Performance;user=Academia;password=martelo;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Avaliacoes>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.DataCriacao).HasColumnType("datetime");

                entity.Property(e => e.DataEdicao).HasColumnType("datetime");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PontosFortes).IsUnicode(false);

                entity.Property(e => e.PontosFracos).IsUnicode(false);

                entity.HasOne(d => d.IdAvaliadorNavigation)
                    .WithMany(p => p.AvaliacoesIdAvaliadorNavigation)
                    .HasForeignKey(d => d.IdAvaliador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Avaliacoes_Utilizadores1");

                entity.HasOne(d => d.IdClassificacaoNavigation)
                    .WithMany(p => p.Avaliacoes)
                    .HasForeignKey(d => d.IdClassificacao)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Avaliacoes_Classificacoes");

                entity.HasOne(d => d.IdProjetoNavigation)
                    .WithMany(p => p.Avaliacoes)
                    .HasForeignKey(d => d.IdProjeto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Avaliacoes_Projetos");

                entity.HasOne(d => d.IdUtilizadorNavigation)
                    .WithMany(p => p.AvaliacoesIdUtilizadorNavigation)
                    .HasForeignKey(d => d.IdUtilizador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Avaliacoes_Utilizadores");
            });

            modelBuilder.Entity<Classificacoes>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.DataCriacao).HasColumnType("datetime");

                entity.Property(e => e.DataEdicao).HasColumnType("datetime");

                entity.Property(e => e.Descricao)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Nota)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserCriacao)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserEdicao)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Clientes>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Empresa)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Nif)
                    .IsRequired()
                    .HasColumnName("NIF")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.Clientes)
                    .HasForeignKey<Clientes>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Clientes_Utilizadores");
            });

            modelBuilder.Entity<Colaboradores>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.Colaboradores)
                    .HasForeignKey<Colaboradores>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Colaboradores_Utilizadores");
            });

            modelBuilder.Entity<Funcoes>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.DataCriacao).HasColumnType("datetime");

                entity.Property(e => e.DataEdicao).HasColumnType("datetime");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserCriacao)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserEdicao)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Notificacoes>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Body)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.DataCriacao).HasColumnType("datetime");

                entity.Property(e => e.DataEdicao).HasColumnType("datetime");

                entity.Property(e => e.Destinatario)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Parametros).IsUnicode(false);

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TipoDestinatario)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoNotificacao)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserCriacao)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserEdicao)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<NotificacoesDesativar>(entity =>
            {
                entity.HasKey(e => new { e.IdUtilizador, e.IdNotificacao });

                entity.Property(e => e.DataDesativacao).HasColumnType("datetime");

                entity.HasOne(d => d.IdNotificacaoNavigation)
                    .WithMany(p => p.NotificacoesDesativar)
                    .HasForeignKey(d => d.IdNotificacao)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NotificacoesDesativar_Notificacoes");

                entity.HasOne(d => d.IdUtilizadorNavigation)
                    .WithMany(p => p.NotificacoesDesativar)
                    .HasForeignKey(d => d.IdUtilizador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NotificacoesDesativar_Utilizadores");
            });

            modelBuilder.Entity<Projetos>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.DataCriacao).HasColumnType("datetime");

                entity.Property(e => e.DataEdicao).HasColumnType("datetime");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UserCriacao)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserEdicao)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.Projetos)
                    .HasForeignKey(d => d.IdCliente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Projetos_Clientes");

                entity.HasOne(d => d.IdGestorNavigation)
                    .WithMany(p => p.Projetos)
                    .HasForeignKey(d => d.IdGestor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Projetos_Utilizadores");
            });

            modelBuilder.Entity<ProjetosColaboradores>(entity =>
            {
                entity.HasKey(e => new { e.IdProjeto, e.IdColaborador });

                entity.HasOne(d => d.IdColaboradorNavigation)
                    .WithMany(p => p.ProjetosColaboradores)
                    .HasForeignKey(d => d.IdColaborador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProjetosColaboradores_Colaboradores");

                entity.HasOne(d => d.IdProjetoNavigation)
                    .WithMany(p => p.ProjetosColaboradores)
                    .HasForeignKey(d => d.IdProjeto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProjetosColaboradores_Projetos");
            });

            modelBuilder.Entity<Utilizadores>(entity =>
            {
                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.DataCriacao).HasColumnType("datetime");

                entity.Property(e => e.DataEdicao).HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Telefone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserCriacao)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserEdicao)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdFuncaoNavigation)
                    .WithMany(p => p.Utilizadores)
                    .HasForeignKey(d => d.IdFuncao)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Utilizadores_Funcoes");
            });
        }
    }
}
