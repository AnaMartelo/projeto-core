﻿using System;
using System.Collections.Generic;

namespace DALCore.Models
{
    public partial class ProjetosColaboradores
    {
        public int IdProjeto { get; set; }
        public int IdColaborador { get; set; }

        public Colaboradores IdColaboradorNavigation { get; set; }
        public Projetos IdProjetoNavigation { get; set; }
    }
}
