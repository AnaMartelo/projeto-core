﻿using System;
using System.Collections.Generic;

namespace DALCore.Models
{
    public partial class Colaboradores
    {
        public Colaboradores()
        {
            ProjetosColaboradores = new HashSet<ProjetosColaboradores>();
        }

        public int Id { get; set; }
        public int IdManager { get; set; }

        public Utilizadores IdNavigation { get; set; }
        public ICollection<ProjetosColaboradores> ProjetosColaboradores { get; set; }
    }
}
