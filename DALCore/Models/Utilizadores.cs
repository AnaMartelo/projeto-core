﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DALCore.Models
{
    public partial class Utilizadores
    {
        public Utilizadores()
        {
            AvaliacoesIdAvaliadorNavigation = new HashSet<Avaliacoes>();
            AvaliacoesIdUtilizadorNavigation = new HashSet<Avaliacoes>();
            NotificacoesDesativar = new HashSet<NotificacoesDesativar>();
            Projetos = new HashSet<Projetos>();
        }

        public int Id { get; set; }
        public int IdFuncao { get; set; }
        [Required]
        [MinLength(5)]
        [MaxLength(15)]
        public string Username { get; set; }
        [Required]
        [MinLength(8)]
        [MaxLength(15)]
        public string Password { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", ErrorMessage= "O email inserido não tem o formato correto.")]
        public string Email { get; set; }
        public string Telefone { get; set; }
        public bool? Active { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataEdicao { get; set; }
        [Required]
        public string UserCriacao { get; set; }
        [Required]
        public string UserEdicao { get; set; }

        public Funcoes IdFuncaoNavigation { get; set; }
        public Clientes Clientes { get; set; }
        public Colaboradores Colaboradores { get; set; }
        public ICollection<Avaliacoes> AvaliacoesIdAvaliadorNavigation { get; set; }
        public ICollection<Avaliacoes> AvaliacoesIdUtilizadorNavigation { get; set; }
        public ICollection<NotificacoesDesativar> NotificacoesDesativar { get; set; }
        public ICollection<Projetos> Projetos { get; set; }
    }
}
