﻿using System;
using System.Collections.Generic;

namespace DALCore.Models
{
    public partial class Funcoes
    {
        public Funcoes()
        {
            Utilizadores = new HashSet<Utilizadores>();
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public bool? Active { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataEdicao { get; set; }
        public string UserCriacao { get; set; }
        public string UserEdicao { get; set; }

        public ICollection<Utilizadores> Utilizadores { get; set; }
    }
}
