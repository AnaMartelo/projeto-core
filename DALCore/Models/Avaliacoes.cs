﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;

namespace DALCore.Models
{[AllowAnonymous]
    public partial class Avaliacoes
    {
        public int Id { get; set; }
        public int IdClassificacao { get; set; }
        public int IdUtilizador { get; set; }
        public int IdAvaliador { get; set; }
        public int IdProjeto { get; set; }
        public string Estado { get; set; }
        public int Mes { get; set; }
        public int Ano { get; set; }
        public string PontosFortes { get; set; }
        public string PontosFracos { get; set; }
        public bool? Active { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataEdicao { get; set; }

        public Utilizadores IdAvaliadorNavigation { get; set; }
        public Classificacoes IdClassificacaoNavigation { get; set; }
        public Projetos IdProjetoNavigation { get; set; }
        public Utilizadores IdUtilizadorNavigation { get; set; }
    }
}
